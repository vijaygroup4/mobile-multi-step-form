//declaring variables globally
let isArcade, isAdvanced, isPro;
let isOnlineService = false;
let isLargerStorage = false;
let isCustomizableProfile = false;
let isMonthly = true;
let isYearly = false;
let onlineServiceMonthly = 0;
let largerStorageMonthly = 0;
let customizableProfileMonthly = 0;
let onlineServiceYearly = 0;
let largerStorageYearly = 0;
let customizableProfileYearly = 0;

//function that displays next form and hides the current form
function displayAndHide(showBlock, hideBlock, showNextButton, hideNextButton, showGoBackButton, hideGoBackButton, addStep, hideStep, useGoBack) {
  showBlock.style.display = "block";
  hideBlock.style.display = "none";
  showNextButton.style.display = "block";
  hideNextButton.style.display = "none";
  showGoBackButton ? (showGoBackButton.style.display = "block") : "none";
  useGoBack ? (hideGoBackButton.style.display = "none") : (hideGoBackButton.style.display = "block");
  addStep.classList.add("active");
  hideStep.classList.remove("active");
}

//function that displays error when there is no selection
function displayFormError(formElement, errorMessage) {
  formElement.querySelector(".form-error").textContent = errorMessage;
  formElement.querySelector(".form-error").style.display = errorMessage ? "block" : "none";
}

//Event Listeners Attached
document.addEventListener("DOMContentLoaded", function () {
  const nameInput = document.getElementById("name");
  const emailInput = document.getElementById("email");
  const phoneNumberInput = document.getElementById("phone_number");
  const nextStep1 = document.querySelector("#next-step-1");

  function displayError(inputElement, errorMessage) {
    inputElement.previousElementSibling.querySelector(".error").style.display = errorMessage ? "block" : "none";
    inputElement.style.borderColor = errorMessage ? "hsl(354, 84%, 57%)" : "";
  }

  // first form
  nextStep1.addEventListener("click", function (event) {
    if (nameInput.value.trim() === "" || !/^[a-zA-Z\s]+$/.test(nameInput.value.trim())) {
      displayError(nameInput, true);
      return;
    } else {
      displayError(nameInput, false);
    }

    if (emailInput.value.trim() === "" || !emailInput.value.trim().includes("@")) {
      displayError(emailInput, true);
      return;
    } else {
      displayError(emailInput, false);
    }

    if (phoneNumberInput.value.trim() === "" || !phoneNumberInput.value.trim().startsWith("+") || phoneNumberInput.value.trim().length !== 13) {
      displayError(phoneNumberInput, true);
      return;
    } else {
      displayError(phoneNumberInput, false);
    }

    // second form
    const form1Content = document.querySelector(".form-1-whole-content");
    const form2Content = document.querySelector(".form-2-whole-content");
    const nextStep2 = document.querySelector("#next-step-2");
    const goBack2 = document.querySelector("#go-back-2");
    const step1 = document.querySelector(".step-1 .circle");
    const step2 = document.querySelector(".step-2 .circle");

    displayAndHide(form2Content, form1Content, nextStep2, nextStep1, null, goBack2, step2, step1);

    const plans = [
      {
        name: "arcade",
        monthlyPrice: "$9/mo",
        yearlyPrice: "$90/yr",
        color: "hsl(231, 11%, 63%)",
      },
      {
        name: "advanced",
        monthlyPrice: "$12/mo",
        yearlyPrice: "$120/yr",
        color: "hsl(231, 11%, 63%)",
      },
      {
        name: "pro",
        monthlyPrice: "$15/mo",
        yearlyPrice: "$150/yr",
        color: "hsl(231, 11%, 63%)",
      },
    ];
    const inputField = document.querySelector("#check");
    inputField.addEventListener("change", function () {
      // Iterate over the plan options and update prices and colors based on the selected subscription type
      plans.forEach((plan) => {
        const planElement = document.querySelector(`.${plan.name}`);
        const priceElement = planElement.querySelector(".price");

        if (inputField.checked) {
          isMonthly = false;
          isYearly = true;
          priceElement.innerHTML = `<div class='inner-price'>${plan.yearlyPrice}</div><p class='months'>2 months free</p>`;
        } else {
          isMonthly = true;
          isYearly = false;
          priceElement.innerHTML = `<div class='inner-price'>${plan.monthlyPrice}</div>`;
        }
      });
    });

    //retrieving the state of the radio buttons and Function to update the outline-purple class based on the plan state
    function updateOutlinePurpleClass(plan, isActive) {
      const planElement = document.querySelector(`.${plan}`);
      let set = isActive ? planElement.classList.add("outline-purple") : planElement.classList.remove("outline-purple");
    }
    // Update outline-purple class for each plan option
    updateOutlinePurpleClass("arcade", isArcade);
    updateOutlinePurpleClass("advanced", isAdvanced);
    updateOutlinePurpleClass("pro", isPro);

    //executing form 2
    const form2 = document.querySelector(".form-2");
    form2.addEventListener("click", function (event) {
      const clickedElement = event.target;

      if (clickedElement.classList.contains("arcade") || clickedElement.closest(".arcade")) {
        isArcade = true;
        document.querySelector(".arcade").classList.add("outline-purple");
      } else {
        isArcade = false;
        document.querySelector(".arcade").classList.remove("outline-purple");
      }

      if (clickedElement.classList.contains("advanced") || clickedElement.closest(".advanced")) {
        isAdvanced = true;
        document.querySelector(".advanced").classList.add("outline-purple");
      } else {
        isAdvanced = false;
        document.querySelector(".advanced").classList.remove("outline-purple");
      }

      if (clickedElement.classList.contains("pro") || clickedElement.closest(".pro")) {
        isPro = true;
        document.querySelector(".pro").classList.add("outline-purple");
      } else {
        isPro = false;
        document.querySelector(".pro").classList.remove("outline-purple");
      }
    });

    //clicking on Go Back button loads previous form
    goBack2.addEventListener("click", function (event) {
      displayAndHide(form1Content, form2Content, nextStep1, nextStep2, goBack2, goBack2, step1, step2, true);
    });

    // third form
    nextStep2.addEventListener("click", function (event) {
      if (!isArcade && !isAdvanced && !isPro) {
        displayFormError(document.querySelector(".form-2-whole-content"), "Please select a plan");
        return;
      } else {
        displayFormError(document.querySelector(".form-2-whole-content"), "");
      }
      const form3Content = document.querySelector(".form-3-whole-content");
      const step3 = document.querySelector(".step-3 .circle");
      const nextStep3 = document.querySelector("#next-step-3");
      const goBack3 = document.querySelector("#go-back-3");
      displayAndHide(form3Content, form2Content, nextStep3, nextStep2, goBack3, goBack2, step3, step2, true);

      const onlineService = document.querySelector(".online-service");
      const largerStorage = document.querySelector(".larger-storage");
      const customizableProfile = document.querySelector(".customizable-profile");

      //based on the price plan selected, the add-ons will be displayed
      if (isMonthly) {
        console.log("isMonthly active");
        document.querySelector(".online-service .price").innerText = `+$1/mo`;
        document.querySelector(".larger-storage .price").innerText = `+$2/mo`;
        document.querySelector(".customizable-profile .price").innerText = `+$2/mo`;
      } else if (isYearly) {
        console.log("isYearly active");
        document.querySelector(".online-service .price").innerText = `+$10/yr`;
        document.querySelector(".larger-storage .price").innerText = `+$20/yr`;
        document.querySelector(".customizable-profile .price").innerText = `+$20/yr`;
      }

      //retrieving the state of the radio buttons
      let onlineServiceSet = isOnlineService == true ? onlineService.classList.add("outline-purple") : onlineService.classList.remove("outline-purple");
      let isLargerStorageSet = isLargerStorage == true ? largerStorage.classList.add("outline-purple") : largerStorage.classList.remove("outline-purple");
      let isCustomizableProfileSet = isCustomizableProfile == true ? customizableProfile.classList.add("outline-purple") : customizableProfile.classList.remove("outline-purple");

      //executing form 3
      const form3 = document.querySelector(".form-3");
      form3.addEventListener("click", function (event) {
        event.stopImmediatePropagation();
        const clickedElement = event.target;

        //adding the outline-purple class to the clicked element
        if (clickedElement.classList.contains("online-service") || clickedElement.closest(".online-service")) {
          isOnlineService = !isOnlineService;
          if (isOnlineService) {
            onlineService.classList.add("outline-purple");
            onlineService.querySelector("#checkbox").checked = true;
            onlineService.querySelector("input").classList.add("svg-color");

            if (isMonthly) {
              onlineServiceMonthly = 1;
            } else {
              onlineServiceYearly = 10;
            }
          } else {
            onlineService.classList.remove("outline-purple");
            onlineService.querySelector("#checkbox").checked = false;
            onlineService.querySelector("input").classList.remove("svg-color");
          }
        }

        if (clickedElement.classList.contains("larger-storage") || clickedElement.closest(".larger-storage")) {
          isLargerStorage = !isLargerStorage;
          if (isLargerStorage) {
            largerStorage.classList.add("outline-purple");
            largerStorage.querySelector("#checkbox").checked = true;
            largerStorage.querySelector("input").classList.add("svg-color");

            if (isMonthly) {
              largerStorageMonthly = 2;
            } else {
              largerStorageYearly = 20;
            }
          } else {
            largerStorage.classList.remove("outline-purple");
            largerStorage.querySelector("#checkbox").checked = false;
            largerStorage.querySelector("input").classList.remove("svg-color");
          }
        }

        if (clickedElement.classList.contains("customizable-profile") || clickedElement.closest(".customizable-profile")) {
          isCustomizableProfile = !isCustomizableProfile;
          if (isCustomizableProfile) {
            customizableProfile.classList.add("outline-purple");
            customizableProfile.querySelector("#checkbox").checked = true;
            customizableProfile.querySelector("input").classList.add("svg-color");

            if (isMonthly) {
              customizableProfileMonthly = 2;
            } else {
              customizableProfileYearly = 20;
            }
          } else {
            customizableProfile.classList.remove("outline-purple");
            customizableProfile.querySelector("#checkbox").checked = false;
            customizableProfile.querySelector("input").classList.remove("svg-color");
          }
        }
      });

      //clicking on Go Back button loads previous form
      goBack3.addEventListener("click", function (event) {
        displayAndHide(form2Content, form3Content, nextStep2, nextStep3, goBack2, goBack3, step2, step3, true);
      });

      //form 4
      nextStep3.addEventListener("click", function (event) {
        if (!isOnlineService && !isLargerStorage && !isCustomizableProfile) {
          displayFormError(document.querySelector(".form-3-whole-content"), "Please select an add-on");
          return;
        } else {
          displayFormError(document.querySelector(".form-3-whole-content"), "");
        }
        const form4Content = document.querySelector(".form-4-whole-content");
        const step4 = document.querySelector(".step-4 .circle");
        const nextStep4 = document.querySelector("#next-step-4");
        const goBack4 = document.querySelector("#go-back-4");

        displayAndHide(form4Content, form3Content, nextStep4, nextStep3, goBack4, goBack3, step4, step3, true);
        let titleContent = ``;
        let titlePrice = ``;
        let totalPrice = 0;
        let firstPrice = ``;
        let secondPrice = ``;
        let thirdPrice = ``;

        // Define plan details using an object
        const planDetails = {
          arcade: { name: "Arcade", monthlyPrice: 9, yearlyPrice: 90 },
          advanced: { name: "Advanced", monthlyPrice: 12, yearlyPrice: 120 },
          pro: { name: "Pro", monthlyPrice: 15, yearlyPrice: 150 },
        };

        // Determine selected plan details
        const selectedPlan = isArcade ? "arcade" : isAdvanced ? "advanced" : isPro ? "pro" : null;

        // If a plan is selected, update titleContent, titlePrice, and totalPrice accordingly
        if (selectedPlan) {
          titleContent = planDetails[selectedPlan].name;
          titlePrice = isMonthly ? `${planDetails[selectedPlan].monthlyPrice}/mo` : `${planDetails[selectedPlan].yearlyPrice}/yr`;
          totalPrice = isMonthly ? planDetails[selectedPlan].monthlyPrice : planDetails[selectedPlan].yearlyPrice;
        }

        if (isMonthly) {
          document.querySelector(".form-4 .monthly-or-yearly").innerText = `${titleContent} (Monthly)`;
          document.querySelector(".form-4 .price").innerText = `$${titlePrice}`;
          if (isOnlineService) {
            firstPrice = `+$1/mo`;
            totalPrice += onlineServiceMonthly;
          }
          if (isLargerStorage) {
            secondPrice = `+$2/mo`;
            totalPrice += largerStorageMonthly;
          }
          if (isCustomizableProfile) {
            thirdPrice = `+$2/mo`;
            totalPrice += customizableProfileMonthly;
          }
        } else if (isYearly) {
          document.querySelector(".form-4 .monthly-or-yearly").innerText = `${titleContent} (Yearly)`;
          document.querySelector(".form-4 .price").innerText = `$${titlePrice}`;
          if (isOnlineService) {
            firstPrice = `+$10/yr`;
            totalPrice += onlineServiceYearly;
          }
          if (isLargerStorage) {
            secondPrice = `+$20/yr`;
            totalPrice += largerStorageYearly;
          }
          if (isCustomizableProfile) {
            thirdPrice = `+$20/yr`;
            totalPrice += customizableProfileYearly;
          }
        }

        // Function to toggle the display of an add-on based on its boolean variable
        function toggleAddonDisplay(variable, addonElement, name, price) {
          if (variable) {
            addonElement.style.display = "flex";
            addonElement.querySelector(".add-on").innerText = name;
            addonElement.querySelector(".price").innerText = price;
          } else {
            addonElement.style.display = "none";
          }
        }

        // Toggle display for each add-on
        toggleAddonDisplay(isOnlineService, document.querySelector(".form-4 .first"), "Online Service", firstPrice);
        toggleAddonDisplay(isLargerStorage, document.querySelector(".form-4 .second"), "Larger Storage", secondPrice);
        toggleAddonDisplay(isCustomizableProfile, document.querySelector(".form-4 .third"), "Customizable Profile", thirdPrice);

        //total price
        document.querySelector(".form-4 .total-para").innerText = `Total (per ${isMonthly ? "month" : "Year"})`;
        document.querySelector(".form-4 .total-price").innerText = `+$${totalPrice}/${isMonthly ? "mo" : "yr"}`;

        //form 5
        nextStep4.addEventListener("click", function (event) {
          nextStep4.style.display = "none";
          goBack4.style.display = "none";
          form4Content.style.display = "none";
          const form5Content = document.querySelector(".form-5-whole-content");
          form5Content.style.display = "block";
        });

        //clicking on Go Back button loads the previous form
        goBack4.addEventListener("click", function (event) {
          displayAndHide(form3Content, form4Content, nextStep3, nextStep4, goBack3, goBack4, step3, step4, true);
        });

        //clicking on Change loads the second form
        document.querySelector(".form-4 a").addEventListener("click", function (event) {
          displayAndHide(form2Content, form4Content, nextStep2, nextStep4, goBack2, goBack4, step2, step4, true);
        });
      });
    });
  });
});
